#!/bin/bash

base_dir='/retail_demo'
local_dir='/home/hduser/datafiles'

#hadoop fs -copyFromLocal /home/akhil/Downloads/Spark_project/datafiles/email_addresses_dim.tsv retail_demo/email_address

# Clean up any previous load
echo "hadoop fs -rm -r -skipTrash $base_dir"
hadoop fs -rm -r -skipTrash $base_dir

echo "hadoop fs -mkdir $base_dir"
hadoop fs -mkdir $base_dir

#for file in $local_dir/*.tsv.gz
for file in $local_dir/*.tsv
do
  dir=`echo $file | perl -ne 's/^(.+?)\..+$/$1/;print;'`
  echo "hadoop fs -mkdir $base_dir/$(basename $dir)"
  hadoop fs -mkdir $base_dir/$(basename $dir)
  echo "hadoop fs -put $file $base_dir/$(basename $dir)/"
  hadoop fs -put $file $base_dir/$(basename $dir)/
done

echo "loading completed"
