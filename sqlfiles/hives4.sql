show databases;
use ${hiveconf:dbname};

CREATE EXTERNAL TABLE et_date_dim_hive
(
  calendar_day      timestamp,
  reporting_year    smallint,
  reporting_quarter smallint,
  reporting_month   smallint,
  reporting_week    smallint,
  reporting_dow     smallint
)
  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
  LOCATION '/retail_demo/date_dim/';
