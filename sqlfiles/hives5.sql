show databases;
use ${hiveconf:dbname};

CREATE EXTERNAL TABLE et_customers_dim_hive
(
  Customer_ID    bigint,
  First_Name     string,
  Last_Name      string,
  Gender         string
)
  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
  LOCATION '/retail_demo/customers_dim/'
