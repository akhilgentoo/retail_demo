show databases;
use ${hiveconf:dbname};

CREATE EXTERNAL TABLE et_customer_addresses_dim_hive
(
  Customer_Address_ID  bigint,
  Customer_ID          bigint,
  Valid_From_Timestamp timestamp,
  Valid_To_Timestamp   timestamp,
  House_Number         string,
  Street_Name          string,
  Appt_Suite_No        string,
  City                 string,
  State_Code           string,
  Zip_Code             string,
  Zip_Plus_Four        string,
  Country              string,
  Phone_Number     string
)
  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
  LOCATION '/retail_demo/customers_dim/';
