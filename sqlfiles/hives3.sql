show databases;
use ${hiveconf:dbname};


CREATE EXTERNAL TABLE et_categories_dim_hive
(
  Category_ID    bigint,
  Category_Name  string
)
  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
  LOCATION '/retail_demo/categories_dim/'; 
