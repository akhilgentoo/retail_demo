

show databases;
use ${hiveconf:dbname};


CREATE EXTERNAL TABLE ${hiveconf:dbname}.${hiveconf:tablename1}
( order_ID                      string
, Order_Item_ID                 bigint
, Product_ID                    int
, Product_Name                  string
, Customer_ID                   int
, Store_ID                      int
, Item_Shipment_Status_Code     string
, Order_Datetime                timestamp
, Ship_Datetime                 timestamp
, Item_Return_Datetime          timestamp
, Item_Refund_Datetime          timestamp
, Product_Category_ID           int
, Product_Category_Name         string
, Payment_Method_Code           string
, Tax_Amount                    double
, Item_Quantity                 int
, Item_Price                    double
, Discount_Amount               double
, Coupon_Code                   string
, Coupon_Amount                 double
, Ship_Address_Line1            string
, Ship_Address_Line2            string
, Ship_Address_Line3            string
, Ship_Address_City             string
, Ship_Address_State            string
, Ship_Address_Postal_Code      string
, Ship_Address_Country          string
, Ship_Phone_Number             string
, Ship_Customer_Name            string
, Ship_Customer_Email_Address   string
, Ordering_Session_ID           string
, Website_URL                   string
)
  --PARTITIONED BY (Order_Datetime timestamp)
  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
  LOCATION '/retail_demo/orders/'; 
