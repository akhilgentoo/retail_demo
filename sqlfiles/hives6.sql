show databases;
use ${hiveconf:dbname};


CREATE EXTERNAL TABLE et_payment_methods_hive
(
  Payment_Method_ID    SMALLINT,
  Payment_Method_Code  string
)
  ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
  LOCATION '/retail_demo/payment_methods/'
