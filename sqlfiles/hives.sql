create database ${hiveconf:dbname};
show databases;
use ${hiveconf:dbname};


CREATE EXTERNAL TABLE ${hiveconf:dbname}.${hiveconf:tablename} 
(Customer_ID  int,Email_Address string) row format delimited fields terminated by '\t' 
STORED AS TEXTFILE location '/retail_demo/bkp_email_addresses/';


